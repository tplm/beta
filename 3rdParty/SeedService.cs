﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3rdParty
{
    class SeedService
    {
        private readonly Dictionary<int, string> data = new Dictionary<int, string>{
            {1, "new"},
            {2, "data"},
            {3, "from"},
            {4, "external"},
            {5, "provider"}
        };

        private readonly ApiClient apiClient;

        public SeedService()
        {
            apiClient = new ApiClient();
        }

        public async Task Reseed()
        {
            Console.WriteLine("Geting existing content");

            var idsToDelete = await GetAllIdsAsync();

            Console.WriteLine("Deleting existing content");

            await Task.WhenAll(idsToDelete.Select(i => DeleteAsync(i)));

            Console.WriteLine("Seeding new data");

            await Task.WhenAll(data.Select(d => PostNewContentAsync(d.Key, d.Value)));
        }

        private async Task<List<int>> GetAllIdsAsync()
        {
            var result = await apiClient.GetStringAsync("https://localhost:5001/api/AuthorizeWithPermission");

            var items = JArray.Parse(result);

            return items.Select(i => i.Value<int>("id")).ToList();
        }
        private async Task DeleteAsync(int id)
        {
            var url = $"https://localhost:5001/api/AuthorizeWithPermission/{id}";
            await apiClient.DeleteAsync(url);
        }

        private async Task PostNewContentAsync(int id, string data)
        {
            var url = $"https://localhost:5001/api/AuthorizeWithPermission";

            var json = $"{{ \"id\" : {id}, \"data\" : \"{data}\" }}";
            await apiClient.PostAsync(url, json);
        }
    }
}
