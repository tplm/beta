﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace _3rdParty
{
    public class ApiClient
    {
        private readonly IConfiguration Configuration;

        private static readonly object tokenLock = new object();
        private static DateTimeOffset _tokenEpirationDate = DateTimeOffset.MinValue;
        private static string _authorizationHeaderValue = null;

        private string AuthorizationHeaderValue
        {
            get
            {
                lock (tokenLock)
                {
                    if (_tokenEpirationDate < DateTimeOffset.UtcNow.AddMinutes(5))
                    {
                        var accessToken = GetAccesTokenAsync().Result;
                        _authorizationHeaderValue = $"Bearer {accessToken}";
                        _tokenEpirationDate = GetExpirationDate(accessToken);
                    }

                    return _authorizationHeaderValue;
                }
            }
        }

        public ApiClient()
        {
            Configuration = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
              .Build();

        }

        private DateTimeOffset GetExpirationDate(string accesToken)
        {
            var tokenPayload = accesToken.Split('.')[1] + "==";
            string decodedJWT = Encoding.UTF8.GetString(Convert.FromBase64String(tokenPayload));
            var exp = JObject.Parse(decodedJWT).Value<long>("exp");
            return DateTimeOffset.FromUnixTimeSeconds(exp);
        }

        private async Task<string> GetAccesTokenAsync()
        {             
            var url = $"https://{Configuration["Auth0:Domain"]}/oauth/token";
            var dict = new Dictionary<string, string> {
                { "grant_type", "client_credentials"},
                { "client_id", Configuration["Auth0:ClientId"]},
                { "client_secret", Configuration["Auth0:ClientSecret"]},
                { "audience",  Configuration["Auth0:Audience"]}
            };
            var client = new HttpClient();
            var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = new FormUrlEncodedContent(dict) };
            var res = await client.SendAsync(req);
            var content = await res.Content.ReadAsStringAsync();
            var accesToken = JObject.Parse(content).Value<string>("access_token");
            return accesToken;
        }




        public async Task<string> GetStringAsync(string url)
        {
            var client = new HttpClient();
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            req.Headers.Add("Authorization", AuthorizationHeaderValue);
            var res = await client.SendAsync(req);
            return await res.Content.ReadAsStringAsync();
        }


        public async Task DeleteAsync(string url)
        {
            var client = new HttpClient();
            var req = new HttpRequestMessage(HttpMethod.Delete, url);
            req.Headers.Add("Authorization", AuthorizationHeaderValue);
            await client.SendAsync(req);
        }


        public async Task PostAsync(string url, string data)
        {
            var client = new HttpClient();
            var req = new HttpRequestMessage(HttpMethod.Post, url)
            {
                Content = new StringContent(data, Encoding.UTF8, "application/json")
            };
            req.Headers.Add("Authorization", AuthorizationHeaderValue);
            await client.SendAsync(req);
        }

    }
}
