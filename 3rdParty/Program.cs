﻿using Microsoft.Extensions.Configuration;
using System;

namespace _3rdParty
{
    class Program
    {
        static void Main(string[] args)
        {
            var seedService = new SeedService();
            seedService.Reseed().Wait();
        }
    }
}
