﻿namespace OurApi.DTOs
{
    public class Content
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }
}
