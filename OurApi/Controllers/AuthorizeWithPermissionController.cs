﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OurApi.DTOs;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace OurApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizeWithPermissionController : ControllerBase
    {

        private static ConcurrentDictionary<int, string> _storage;

        private static ConcurrentDictionary<int, string> storage
        {
            get
            {
                if(_storage == null)
                {
                    _storage = new ConcurrentDictionary<int, string>();

                    _storage.TryAdd(1, "some");
                    _storage.TryAdd(10, "data");                    

                }
                return _storage;
            }
        }



        [HttpGet]
        [Authorize]
        public ActionResult<List<Content>> Private()
        {
            return Ok(GetContentList());
        }

        [HttpPost]
        [Authorize("create:content")]
        public ActionResult<string> Create(Content content)
        {
            if(storage.TryAdd(content.Id, content.Data))
            {
                return Ok($"{content.Data} - successfully added");
            }
            else
            {
                return Conflict($"{content.Data} - already exists");
            }

           
        }

        [HttpPut]
        [Authorize("update:content")]
        public ActionResult<string> Update(Content content)
        {
            storage[content.Id] = content.Data;

            return Ok($"{content.Id} - updated");
        }

        [HttpDelete]
        [Authorize("delete:content")]
        [Route("{id}")]
        public ActionResult<string> Delete(int id)
        {
            string removedValue;

            if (storage.TryRemove(id, out removedValue))
            {
                return Ok($"{removedValue} - successfully removed");
            }
            else
            {
                return NotFound($"{id} - object not found");
            }
        }

        private List<Content> GetContentList()
        {
            return storage.Select(kvp => new Content { Id = kvp.Key, Data = kvp.Value }).OrderBy(c => c.Id).ToList();
        }
    }
}
