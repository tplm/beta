﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace OurApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        [HttpGet]
        [Route("public")]
        [AllowAnonymous]
        public ActionResult<string> Public()
        {      
            return Ok(User.Identity.Name ?? "unknown");
        }


        [HttpGet]        
        [Route("private")]
        public ActionResult<string> Private()
        {
            return Ok(User.Identity.Name ?? "unknown");
        }
    }
}
