﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace OurApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SimpleAuthorizeController : ControllerBase
    {

        [HttpGet]
        [Route("public")]
        public ActionResult<string> Public()
        {
            return Ok("You are allways wellcome here");
        }

        [HttpGet]
        [Authorize]
        [Route("private")]
        public ActionResult<string> Private()
        {
            return Ok("this metod require autorization");
        }

    }
}
