﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using OurApi.Auth0;

namespace OurApi
{
    public partial class Startup
    {
        public void ConfigureAuth0(IServiceCollection services)
        {
            var domain = $"https://{Configuration["Auth0:Domain"]}/";

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.Authority = domain;
                options.Audience = Configuration["Auth0:Audience"];
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("create:content", policy => policy.Requirements.Add(new HasPermissionRequirement("create:content", domain)));
                options.AddPolicy("delete:content", policy => policy.Requirements.Add(new HasPermissionRequirement("delete:content", domain)));
                options.AddPolicy("update:content", policy => policy.Requirements.Add(new HasPermissionRequirement("update:content", domain)));
            });

            services.AddSingleton<IAuthorizationHandler, HaspermissionHandler>();
        }
    }
}
