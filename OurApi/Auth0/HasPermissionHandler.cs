﻿using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;

namespace OurApi.Auth0
{
    public class HaspermissionHandler : AuthorizationHandler<HasPermissionRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasPermissionRequirement requirement)
        {
            // If user does not have the permissions claim, get out of here
            if (!context.User.HasClaim(c => c.Type == "permissions" && c.Issuer == requirement.Issuer))
                return Task.CompletedTask;

            // Succeed if the claims array contains the required permission
            if (context.User.Claims.Any(c => c.Type == "permissions" && c.Value == requirement.Permission && c.Issuer == requirement.Issuer))
                context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
